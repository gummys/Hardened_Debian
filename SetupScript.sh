#!/bin/bash	

###################
#####__USER__######
###################
USER="";

###################
####__IP_ADDR__####
###################
IP_ADDR="";

###################
###__PRIV_KEY__####
###################
PRIV_KEY="";

########################
##########_SCP_#########
########################
scp .bashrc ${USER}@${IP_ADDR}:~ -i ${PRIV_KEY}
scp sshd_config ${USER}@${IP_ADDR}:~ -i ${PRIV_KEY}
scp sysctl.conf ${USER}@${IP_ADDR}:/etc/ -i ${PRIV_KEY}

############################################
###__DISABLE_SHORT_DIFFIE_HELLMEN_MODULI__##
############################################

cp /etc/ssh/moduli /etc/ssh/moduli.backup
awk '$5 > 12287' /etc/ssh/moduli > /etc/ssh/moduli.tmp
mv /etc/ssh/moduli.tmp /etc/ssh/moduli
